package com.androidframelearn.util_xutils;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

@ContentView(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewInject(R.id.btn_test)
    private Button btn_test;

    @ViewInject(R.id.btn_test2)
    private Button btn_test2;

    @ViewInject(R.id.tv_test)
    private TextView tv_test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
    }

    @Event(value = {R.id.btn_test,R.id.btn_test2},type = View.OnClickListener.class)
    private void onClick(View view){
        switch (view.getId()){
            case R.id.btn_test:
                tv_test.setText("修改成了测试文本");
                break;
            case R.id.btn_test2:
                tv_test.setText("修改成了测试文本2");
                break;
        }
    }
}
