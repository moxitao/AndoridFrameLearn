package com.androidframelearn.event_asynctask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // 定义ProgressBar
    private ProgressBar pb_test;

    // 定义文本控件
    private TextView tv_test;

    // 定义按钮
    private Button btn_test;

    // 定义ProgressAsyncTask
    private ProgressAsyncTask mProgressAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化UI
        initUI();

        // 注册点击事件
        btn_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressAsyncTask = new ProgressAsyncTask(pb_test, tv_test);
                mProgressAsyncTask.execute();
            }
        });
    }

    /**
     * 初始化UI
     */
    private void initUI() {
        pb_test = findViewById(R.id.pb_test);
        tv_test = findViewById(R.id.tv_test);
        btn_test = findViewById(R.id.btn_test);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 关闭AsyncTask，防止内存泄漏
        mProgressAsyncTask.cancel(true);
    }
}
