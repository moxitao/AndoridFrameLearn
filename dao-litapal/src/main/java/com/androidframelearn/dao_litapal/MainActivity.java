package com.androidframelearn.dao_litapal;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.litepal.LitePal;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btn_db_create;

    private Button btn_db_query;

    private Button btn_db_insert;

    private Button btn_db_update;

    private Button btn_db_delete;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1.初始化UI
        initUI();

        // 2.创建数据库
        createDBbyLitePal();

        // 3.查询数据
        queryDatabyLitePal();

        // 4.插入数据
        insertDatabyLitePal();

        // 5.更新数据
        updateDatabyLitePal();

        // 6.删除数据
        deleteDatabyLitePal();
    }



    /**
     * 1.初始化UI
     */
    private void initUI() {
        btn_db_create = findViewById(R.id.btn_db_create);
        btn_db_query = findViewById(R.id.btn_db_query);
        btn_db_insert = findViewById(R.id.btn_db_insert);
        btn_db_update = findViewById(R.id.btn_db_update);
        btn_db_delete = findViewById(R.id.btn_db_delete);
    }

    /**
     * 2.创建数据库
     */
    private void createDBbyLitePal() {
        btn_db_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"创建数据库成功");
                LitePal.getDatabase();
            }
        });
    }

    /**
     * 3.查询数据
     */
    private void queryDatabyLitePal() {
        btn_db_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Book> books = LitePal.findAll(Book.class);
                for (Book book : books){
                    Log.i(TAG,"查询数据成功");
                    Log.d("MainActivity","书名是"+book.getName());
                    Log.d("MainActivity","书的作者是"+book.getAuthor());
                    Log.d("MainActivity","书的页数是"+book.getPages());
                    Log.d("MainActivity","书的价格是"+book.getPrice());
                }
            }
        });
    }

    /**
     * 4.插入数据
     */
    private void insertDatabyLitePal() {
        btn_db_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book();
                book.setName("The Da Vinci Code");
                book.setauthor("Dan Brown");
                book.setPages(454);
                book.setPrice(16.96);
                book.save();
                Log.i(TAG,"插入数据成功");
            }
        });
    }

    /**
     * 5.更新数据
     */
    private void updateDatabyLitePal() {
        btn_db_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book();
                book.setName("The Lost Symbol");
                book.setauthor("Dan Brown");
                book.setPages(510);
                book.setPrice(19.95); // 第一次设置商品价格
                book.save();
                book.setPrice(10.99); // 第二次设置商品价格
                book.save();
                Log.i(TAG,"更新数据成功");
            }
        });
    }

    /**
     * 6.删除数据
     */
    private void deleteDatabyLitePal() {
        btn_db_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LitePal.deleteAll(Book.class,"price < ?","15");
                Log.i(TAG,"删除成功");
            }
        });
    }
}
