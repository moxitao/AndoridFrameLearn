package com.androidframelearn.event_eventbus;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;

public class PublisherActivity extends AppCompatActivity {

    private Button btn_send_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publisher);

        // 初始化UI
        initUI();

        // 为按钮注册点击事件，发送消息
        btn_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventMessage message = new EventMessage("测试消息","你好！");
                EventBus.getDefault().post(message);
            }
        });
    }

    /**
     * 初始化控件
     */
    private void initUI() {
        btn_send_message = findViewById(R.id.btn_send_message);
    }
}
