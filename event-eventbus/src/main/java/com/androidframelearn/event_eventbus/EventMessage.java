package com.androidframelearn.event_eventbus;

public class EventMessage {

    private String type;
    private String message;

    public EventMessage() {
    }

    public EventMessage(String type, String message) {
        this.type = type;
        this.message = message;
    }

    @Override
    public String toString() {
        return "消息类型为：" + type + "===消息为：" + message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}