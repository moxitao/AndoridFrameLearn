package com.androidframelearn.event_eventbus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class SubscriberActivity extends AppCompatActivity {

    private static final String TAG = "SubscriberActivity";

    private Button btn_next_activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber);

        // 初始化UI
        initUI();

        // 注册点击事件
        btn_next_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriberActivity.this,PublisherActivity.class);
                startActivity(intent);
            }
        });

    }

    /**
     * 初始化UI
     */
    private void initUI() {
        btn_next_activity = findViewById(R.id.btn_next_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // 订阅者——Subscriber需要绑定EventBus
        EventBus.getDefault().register(this);
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        // 订阅者——Subscriber需要解绑EventBus
//        EventBus.getDefault().unregister(this);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 订阅者——Subscriber需要解绑EventBus
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventMessage message) {
        Log.i(TAG,"收到消息啦！" + "消息的类型为：" + message.getType() + "消息的内容为：" + message.getMessage());
    };
}
