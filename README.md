# Android框架学习

### 介绍
AFL：全称为Android Frame Learn，该项目为整合了多个框架demo的杂烩型app，每个模块对应一个框架，实现单独运行和不影响其他模块，每个模块中都包含有一些对于该框架的简单api调用，适合初学者快速集成和上手某个框架，同时也有对应的博文说明

### 包含
#### 1.数据操作层
- Litepal（dao-litepal）：https://blog.csdn.net/qq_41151659/article/details/105894193
- greenDao（dao-greendao）：https://blog.csdn.net/qq_41151659/article/details/105923497

#### 2.网络层
- OkHttp（http-okhttp）：https://blog.csdn.net/qq_41151659/article/details/105935339
- OkIo（http-okio）：https://blog.csdn.net/qq_41151659/article/details/105955205
- Retrofit（http-retrofit）：https://blog.csdn.net/qq_41151659/article/details/105974804

#### 3.工具层
- ButterKnife（util-butterknife）：https://blog.csdn.net/qq_41151659/article/details/105978543
- IcePick（util-icepick）：https://blog.csdn.net/qq_41151659/article/details/105996750
- Dagger（util-dagger）：https://blog.csdn.net/qq_41151659/article/details/106042419
- xUtils（util-xUtils）：https://blog.csdn.net/qq_41151659/article/details/106051361

#### 4.图片加载
- Glide（image-glide）：https://blog.csdn.net/qq_41151659/article/details/106066969
- Fresco（image-fresco）：https://blog.csdn.net/qq_41151659/article/details/106131283

#### 5.消息机制
- Handler（event-handler）：https://blog.csdn.net/qq_41151659/article/details/106156797
- AsyncTask（event-asynctask）：https://blog.csdn.net/qq_41151659/article/details/106156970
- EventBus（event-eventbus）：https://blog.csdn.net/qq_41151659/article/details/106153263
- RxJava（event-rxjava）：https://blog.csdn.net/qq_41151659/article/details/106237461