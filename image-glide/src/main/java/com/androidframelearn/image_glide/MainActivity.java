package com.androidframelearn.image_glide;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private ImageView iv_standard;

    private Button btn_load_image;

    private Button btn_unload_image;

    private File mImageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化UI
        initUI();

        // 为mImageFile赋予实例对象
        mImageFile = new File(this.getExternalFilesDir(null) + File.separator + "test.jpg");

        // 1.加载图片
        loadImage();

        // 2.取消图片加载
        unloadImage();
    }

    /**
     * 初始化UI
     */
    private void initUI() {
        iv_standard = findViewById(R.id.iv_standard);
        btn_load_image = findViewById(R.id.btn_load_image);
        btn_unload_image = findViewById(R.id.btn_unload_image);
    }

    /**
     * 1.加载图片
     */
    private void loadImage() {
        btn_load_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(getApplicationContext())
                        .load(mImageFile)
                        .into(iv_standard);
            }
        });
    }

    /**
     * 2.取消图片加载
     */
    private void unloadImage(){
        btn_unload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(getApplicationContext())
                        .clear(iv_standard);
            }
        });
    }
}