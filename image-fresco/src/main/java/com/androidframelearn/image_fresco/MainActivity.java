package com.androidframelearn.image_fresco;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {

    private SimpleDraweeView iv_standard;

    private Button btn_load_image;

    Uri mUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化UI
        initUI();

        // 为Uri赋予实例对象
        mUri = Uri.parse("http://10.0.2.2:8080/tomcat.png");

        // 1.加载图片
        loadImage();

    }

    /**
     * 初始化UI
     */
    private void initUI() {
        iv_standard = findViewById(R.id.iv_standard);
        btn_load_image = findViewById(R.id.btn_load_image);
    }

    /**
     * 1.加载图片
     */
    private void loadImage() {
        btn_load_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("MainActivity","调用了这个方法！");
                iv_standard.setImageURI(mUri);
            }
        });
    }
}