package com.androidframelearn.dao_greendao;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btn_db_create;

    private Button btn_db_query;

    private Button btn_db_insert;

    private Button btn_db_update;

    private Button btn_db_delete;

    private static final String TAG = "MainActivity";

    private DaoSession mDaoSession;

    private BookDao mBookDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1.初始化UI
        initUI();

        // 2.创建数据库
        createDBbyGreenDao();

        // 3.查询数据
        queryDatabyGreenDao();

        // 4.插入数据
        insertDatabyGreenDao();

        // 5.更新数据
        updateDatabyGreenDao();

        // 6.删除数据
        deleteDatabyGreenDao();
    }



    /**
     * 1.初始化UI
     */
    private void initUI() {
        btn_db_create = findViewById(R.id.btn_db_create);
        btn_db_query = findViewById(R.id.btn_db_query);
        btn_db_insert = findViewById(R.id.btn_db_insert);
        btn_db_update = findViewById(R.id.btn_db_update);
        btn_db_delete = findViewById(R.id.btn_db_delete);
    }

    /**
     * 2.创建数据库
     */
    private void createDBbyGreenDao() {
        btn_db_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 获取daosession对象
                mDaoSession = MyApplication.getInstances().getDaoSession();
                mBookDao = mDaoSession.getBookDao();
                Log.i(TAG,"创建数据库成功");
            }
        });
    }

    /**
     * 3.查询数据
     */
    private void queryDatabyGreenDao() {
        btn_db_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDaoSession != null){
                    List<Book> books = mBookDao.loadAll();
                    for (Book book : books){
                        Log.i(TAG,"查询数据成功");
                        Log.d("MainActivity","书名是"+book.getName());
                        Log.d("MainActivity","书的作者是"+book.getAuthor());
                        Log.d("MainActivity","书的页数是"+book.getPages());
                        Log.d("MainActivity","书的价格是"+book.getPrice());
                    }
                }else {
                    Log.i(TAG,"查询数据失败");
                }
            }
        });
    }

    /**
     * 4.插入数据
     */
    private void insertDatabyGreenDao() {
        btn_db_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDaoSession != null){
                    Book book = new Book();
                    book.setName("The Da Vinci Code");
                    book.setAuthor("Dan Brown");
                    book.setPages(454);
                    book.setPrice(16.96);
                    mBookDao.insert(book);
                    Log.i(TAG,"插入数据成功");
                }else {
                    Log.i(TAG,"插入数据失败");
                }
            }
        });
    }

    /**
     * 5.更新数据
     */
    private void updateDatabyGreenDao() {
        btn_db_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDaoSession != null){
                    Book book = new Book();
                    book.setId(0);
                    book.setName("The Da Vinci Code");
                    book.setAuthor("Dan Brown");
                    book.setPages(100);
                    book.setPrice(16.96);
                    mBookDao.update(book);
                    Log.i(TAG,"更新数据成功");
                }else {
                    Log.i(TAG,"更新数据失败");
                }
            }
        });
    }

    /**
     * 6.删除数据
     */
    private void deleteDatabyGreenDao() {
        btn_db_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBookDao.deleteAll();
                Log.i(TAG,"删除数据失败");
            }
        });
    }
}
