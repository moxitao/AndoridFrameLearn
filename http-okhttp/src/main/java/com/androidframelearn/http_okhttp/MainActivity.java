package com.androidframelearn.http_okhttp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private Button btn_get_syn;

    private Button btn_get_asy;

    private Button btn_post_syn;

    private Button btn_post_asy;

    private static final String TAG = "MainActivity";

    private static final String URL = "http://wwww.baidu.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化UI
        initUI();

        // 同步发送get请求
        getBySynchronized();

        // 异步发送get请求
        getByAsynchronized();

        // 同步发送post请求
        postBySynchronized();

        // 异步发送post请求
        postByAsynchronized();
    }



    /**
     * 初始化UI
     */
    private void initUI() {
        btn_get_syn = findViewById(R.id.btn_get_syn);
        btn_get_asy = findViewById(R.id.btn_get_asy);
        btn_post_syn = findViewById(R.id.btn_post_syn);
        btn_post_asy = findViewById(R.id.btn_post_asy);
    }

    /**
     * 同步发送get请求
     */
    private void getBySynchronized() {
        btn_get_syn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.构建Client对象
                OkHttpClient client = new OkHttpClient();
                // 2.采用建造者模式和链式调用构建Request对象
                final Request request = new Request.Builder()
                        .url(URL) // 请求URL
                        .get() // 默认就是get请求，可以不写
                        .build();
                // 3.通过1和2产生的Client和Request对象生成Call对象
                final Call call = client.newCall(request);
                // 4.同步发送get请求需要使用execute()方法，并且为了防止主线程阻塞需要放在子线程中自行
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // 6.构建response对象
                            Response response = call.execute();
                            Log.d(TAG, "同步发送get请求成功！请求到的信息为：" + response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    /**
     * 异步发送get请求
     */
    private void getByAsynchronized() {
        btn_get_asy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.构建Client对象
                OkHttpClient client = new OkHttpClient();
                // 2.采用建造者模式和链式调用构建Request对象
                final Request request = new Request.Builder()
                        .url(URL) // 请求URL
                        .get() // 默认就是get请求，可以不写
                        .build();
                // 3.通过1和2产生的Client和Request对象生成Call对象
                Call call = client.newCall(request);
                // 4.调用Call对象的enqueue()方法，并且实现一个回调实现类
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        Log.d(TAG, "异步发送get请求失败！");
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        Log.d(TAG, "异步发送get请求成功！请求到的信息为：" + response.body().string());
                    }
                });
            }
        });
    }

    /**
     * 同步发送post请求
     */
    private void postBySynchronized() {
        btn_post_syn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.构建Client对象
                OkHttpClient client = new OkHttpClient();
                // 2.采用建造者模式和链式调用构建键值对对象
                FormBody formBody = new FormBody.Builder()
                        .add("username", "admin")
                        .add("password", "123456")
                        .build();
                // 3.采用建造者模式和链式调用构建Request对象
                final Request request = new Request.Builder()
                        .url(URL) // 请求URL
                        .post(formBody) // 默认就是get请求，可以不写
                        .build();
                // 4.通过1和3产生的Client和Request对象生成Call对象
                final Call call = client.newCall(request);
                // 5.同步发送post请求需要使用execute()方法，并且为了防止主线程阻塞需要放在子线程中自行
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            // 6.构建response对象
                            Response response = call.execute();
                            Log.d(TAG, "同步发送post请求成功！请求到的信息为：" + response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }

    /**
     * 异步发送post请求
     */
    private void postByAsynchronized() {
        btn_post_asy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.构建Client对象
                OkHttpClient client = new OkHttpClient();
                // 2.采用建造者模式和链式调用构建键值对对象
                FormBody formBody = new FormBody.Builder()
                        .add("username", "admin")
                        .add("password", "123456")
                        .build();
                // 3.采用建造者模式和链式调用构建Request对象
                final Request request = new Request.Builder()
                        .url(URL) // 请求URL
                        .post(formBody) // 默认就是get请求，可以不写
                        .build();
                // 4.通过1和3产生的Client和Request对象生成Call对象
                Call call = client.newCall(request);
                // 5.调用Call对象的enqueue()方法，并且实现一个回调实现类
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        Log.d(TAG, "异步发送post请求失败！");
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        Log.d(TAG, "异步发送post请求成功！请求到的信息为：" + response.body().string());
                    }
                });
            }
        });
    }
}