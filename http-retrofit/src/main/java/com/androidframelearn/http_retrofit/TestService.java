package com.androidframelearn.http_retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TestService {

    /**
     * 1.通过拼接网络参数的方式提交GET请求（下面的参数为https://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=15878896543）
     * 注意：这种方式为http的参数传递方式，若使用这种方式请求，需要在注解上使用@Query而非@Path
     * @param tel
     * @return
     */
    @GET("mobile_tel_segment.htm")
    Call<ResponseBody> getInfoByHttp(@Query("tel") String tel);

    /**
     * 2.通过restful的方式提交GET请求（下面的参数为http://10.0.2.2:8080/update74.json）
     * @param filename
     * @return
     */
    @GET("{filename}")
    Call<ResponseBody> getInfoByRestFul(@Path("filename") String filename);

    /**
     * 3.通过restful的方式提交POST请求（下面的参数为http://10.0.2.2:8080/update74.json）
     * @param filename
     * @return
     */
    @POST("{filename}")
    Call<ResponseBody> postInfoByRestFul(@Path("filename") String filename);
}
