package com.androidframelearn.http_retrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private Button btn_get;

    private Button btn_get_restful;

    private Button btn_post;

    private static final String TAG = "MainActivity";

    private static final String URL = "https://tcc.taobao.com/cc/json/";

    private static final String LOCALURL = "http://10.0.2.2:8080/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始化UI
        initUI();

        // 发送GET请求
        getByRetrogit();

        // 发送GET请求——通过restful风格
        getByRetrogitForRestful();

        // 发送POST请求
        postByRetrogit();
    }

    /**
     * 初始化UI
     */
    private void initUI() {
        btn_get = findViewById(R.id.btn_get);
        btn_get_restful = findViewById(R.id.btn_get_restful);
        btn_post = findViewById(R.id.btn_post);
    }

    /**
     * 发送GET请求
     */
    private void getByRetrogit() {
        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.使用建造者模式创建Retrofit实例
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(URL)
                        .build();
                // 2.使用Retrofit对象调用create()创建服务实例（BookService）
                TestService testService = retrofit.create(TestService.class);
                // 3.通过服务实例创建Call对象
                Call<ResponseBody> call = testService.getInfoByHttp("15878896543");
                // 4.通过Call对象构建网络请求（异步）
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String output = response.body().string();
                            Log.i(TAG,"GET请求发送成功！获取到的信息为：" + output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i(TAG,"GET请求发送失败");
                    }
                });
            }
        });
    }

    /**
     * 发送GET请求——通过restful风格
     */
    private void getByRetrogitForRestful() {
        btn_get_restful.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.使用建造者模式创建Retrofit实例
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(LOCALURL)
                        .build();
                // 2.使用Retrofit对象调用create()创建服务实例（BookService）
                TestService testService = retrofit.create(TestService.class);
                // 3.通过服务实例创建Call对象
                Call<ResponseBody> call = testService.getInfoByRestFul("update74.json");
                // 4.通过Call对象构建网络请求（异步）
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String output = response.body().string();
                            Log.i(TAG,"GET请求发送成功！获取到的信息为：" + output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i(TAG,"GET请求发送失败");
                    }
                });
            }
        });
    }

    /**
     * 发送POST请求
     */
    private void postByRetrogit() {
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1.使用建造者模式创建Retrofit实例
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(LOCALURL)
                        .build();
                // 2.使用Retrofit对象调用create()创建服务实例（BookService）
                TestService testService = retrofit.create(TestService.class);
                // 3.通过服务实例创建Call对象
                Call<ResponseBody> call = testService.postInfoByRestFul("update74.json");
                // 4.通过Call对象构建网络请求（异步）
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String output = response.body().string();
                            Log.i(TAG,"POST请求发送成功！获取到的信息为：" + output);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i(TAG,"POST请求发送失败");
                    }
                });
            }
        });
    }

}