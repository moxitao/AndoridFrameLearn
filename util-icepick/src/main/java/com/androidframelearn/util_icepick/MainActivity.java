package com.androidframelearn.util_icepick;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import icepick.Icepick;
import icepick.State;

public class MainActivity extends AppCompatActivity {

    private Button btn_add_number;

    private TextView tv_number;

    @State
    int mNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 使用IcePick读取数据
        Icepick.restoreInstanceState(this, savedInstanceState);
        // 获取控件实例
        btn_add_number = findViewById(R.id.btn_add_number);
        tv_number = findViewById(R.id.tv_number);
        // 初始化文本控件
        tv_number.setText(mNumber + "");
        // 为按钮注册点击事件
        btn_add_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_number.setText(++mNumber + "");
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        // 使用IcePick存入数据
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }
}
