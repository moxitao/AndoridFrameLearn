package com.androidframelearn.util_butterknife;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_test_one)
    Button btn_test_one;

    @BindView(R.id.btn_test_two)
    Button btn_test_two;

    @BindView(R.id.tv_test)
    TextView tv_test;

    @BindString(R.string.test_number)
    String test_number;

    @BindString(R.string.test_latter)
    String test_latter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 绑定ButterKnife
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_test_one, R.id.btn_test_two})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_test_one:
                tv_test.setText(test_number);
                break;
            case R.id.btn_test_two:
                tv_test.setText(test_latter);
                break;
            default:
                break;
        }
    }
}