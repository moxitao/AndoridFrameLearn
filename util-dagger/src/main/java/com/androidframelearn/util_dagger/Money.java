package com.androidframelearn.util_dagger;

import javax.inject.Inject;

public class Money {

    private int total;

    public Money() {
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
