package com.androidframelearn.util_dagger;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ManModule.class)
public interface ManComponent {
    void inject(MainActivity MainActivity);
}
