package com.androidframelearn.util_dagger;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Inject
    Man man;

    @Inject
    Money money;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DaggerManComponent.builder().build().inject(this);
        man.setName("小明");
        man.setMoney(money);
        Log.i(TAG,"当前人物：" + man.getName() + "=========拥有钱数：" + man.getMoney().getTotal());
    }
}