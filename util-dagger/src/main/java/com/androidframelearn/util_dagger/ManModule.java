package com.androidframelearn.util_dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ManModule {

    @Singleton
    @Provides
    Money provideMoney(){
        Money money = new Money();
        money.setTotal(10000);
        return money;
    }
}
