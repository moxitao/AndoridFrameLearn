package com.androidframelearn.util_dagger;

import javax.inject.Inject;

public class Man {

    private String name;

    private Money money;

    @Inject
    public Man() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Money getMoney() {
        return money;
    }

    public void setMoney(Money money) {
        this.money = money;
    }
}
